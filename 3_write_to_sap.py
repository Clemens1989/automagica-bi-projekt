# -*- coding: utf-8 -*-

from automagica import *
import pandas as pd
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import datetime, time, smtplib, re, os
from selenium.webdriver.common.keys import Keys


DESKTOP = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop') 
DATA_PATH = DESKTOP + r"\Bestellungen\CSV\\"


def handle_order(orderData):
    browser = Chrome()
    browser.get("https://m29z.ucc.ovgu.de/sap/bc/gui/sap/its/webgui?sap-client=231&sap-language=DE")
    login_to_SAP(browser)
    open_VA01(browser)
    verkaufsbelege_anlegen(browser)
    fill_out_order(browser,orderData)
    auftragsnummer = save_order_return_number(browser)
    send_mail(auftragsnummer,orderData )
    browser.close()

def send_mail(auftragsnummer,orderData ):
    user = "saschasuper55@gmail.com"
    host = "imap.gmail.com"
    password = get_credential(username=user, system=host)
    to = orderData['Email'][0]
    SUBJECT = "Bestellung " + str(auftragsnummer)
    TEXT = "Danke für Ihre Bestellung. Die Nummer lautet: " + str(auftragsnummer)

    # Prepare actual message
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (user, to, SUBJECT, TEXT)
        
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    server.starttls()
    server.login(user,password)
    server.sendmail(user, to, message.encode('utf-8').strip())
    server.quit()

def login_to_SAP(browser):
    wait_till_page_loaded(browser,10,"sap-user")
    browser.find_element_by_id("sap-user").send_keys("LEARN-100")
    browser.find_element_by_id("sap-password").send_keys("bimaster")
    browser.find_element_by_id("LOGON_BUTTON").click()
    
def open_VA01(browser):
    actionChains = ActionChains(browser)
    wait_till_page_loaded(browser,10,"tree#105#3#1#1#i")
    actionChains.double_click(browser.find_element_by_id("tree#105#3#1#1#i")).perform()
    
def verkaufsbelege_anlegen(browser):
    wait_till_page_loaded(browser,10,"M0:46:::2:22")
    browser.find_element_by_id("M0:46:::2:22").send_keys("TA")
    browser.find_element_by_id("M0:50::btn[0]").click()
    
def fill_out_order(browser,orderData):
    wait_till_page_loaded(browser,10,"M0:46:1:1::0:17")
    browser.find_element_by_id("M0:46:1:1::0:17").send_keys(int(orderData["Kundennummer"][0]))
    browser.find_element_by_id("M0:46:1:1::1:17").send_keys(int(orderData["Kundennummer"][0]))
    browser.find_element_by_id("M0:46:2:3B256:2:1::0:3").click()
    wait_till_page_loaded(browser,10,"M1:46:::4:15_l")
    browser.find_element_by_id("M1:46:::4:15_l").click()
    browser.find_element_by_id("M1:50::btn[0]").click()
    wait_till_element_disappeared(browser,10,"M1:50::btn[0]")
    browser.find_element_by_id("M0:46:1::3:17").send_keys(int(orderData["Bestellnummer"][0]))
    date = datetime.datetime.strptime(orderData["Bestelldatum"][0], "%d.%m.%Y").strftime('%m/%d/%Y')
    browser.find_element_by_id("M0:46:1::3:56").send_keys(date)
    browser.find_element_by_id("M0:46:2:3B256:2:1::0:3").click()
    time.sleep(2)
    for index, row in orderData.iterrows():
        
        browser.switch_to.active_element.send_keys(row["Art-Nummer"])
        actions = ActionChains(browser)
        actions.send_keys(Keys.TAB * 2).perform()
        browser.switch_to.active_element.send_keys(row["Menge"])

        #actions.send_keys(Keys.TAB * 7).perform()
        #date = datetime.datetime.strptime(row["Wunsch-Lieferdatum"], "%d.%m.%Y").strftime('%m/%d/%Y')
        #browser.switch_to.active_element.send_keys(date)
        actions = ActionChains(browser)
        actions.send_keys(Keys.ENTER).perform()
        time.sleep(1)
        if browser.find_element_by_id("M0:35").text == "Terminauftrag: Verfügbarkeitskontrolle":
            browser.find_element_by_id("M0:46:::16:80").click()
            wait_till_page_loaded(browser,10,"M0:46:2:3B256:2:1::0:3")
    
def save_order_return_number(browser):
    browser.find_element_by_id("M0:50::btn[11]").click()
    wait_till_page_loaded(browser,120,"wnd[0]/sbar_msg-txt")
    result = browser.find_element_by_id("wnd[0]/sbar_msg-txt").text
    return re.findall("\d+", result)[0]
   

def wait_till_page_loaded(browser,delay,id):
    try:
        WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.ID, id)))
    except TimeoutException:
        print("Loading took to long!")

def wait_till_element_disappeared(browser,delay,id):
    try:
        WebDriverWait(browser, delay).until(EC.invisibility_of_element_located((By.ID, id)))
    except TimeoutException:
        print("Loading took to long!")

def main():
    orders = get_files_in_folder(DATA_PATH, extension=".csv", show_full_path=True, scan_subfolders=False)
    for order in orders:
        orderData = pd.read_csv(order)
        handle_order(orderData)

main()
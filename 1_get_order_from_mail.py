# -*- coding: utf-8 -*-


import  sys
import requests, PyPDF2, io, tabula, easyimap, os
from automagica import *
import xml.etree.ElementTree as ET

DESKTOP = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop') 
ORDER_PATH = DESKTOP + r"\Bestellungen\neu\\"


# die Funktion erstellt eine Ordnerstruktur auf dem Desktop
def create_folder_structure():
    if not os.path.exists(ORDER_PATH):
        os.makedirs(ORDER_PATH)

# die Funktion speoichert eine PDF Datei in einen Ordner
def save_pdf_attachement_to_folder(attachementData, folder_path= ORDER_PATH):
    pdf_content = io.BytesIO(attachementData[1])
    pdf_reader = PyPDF2.PdfFileReader(pdf_content)
    pdfWriter = PyPDF2.PdfFileWriter()
    for pageNum in range(pdf_reader.numPages):
        pdfWriter.addPage(pdf_reader.getPage(pageNum))
    resultPdf = open(folder_path + generate_unique_identifier() + attachementData[0] , 'wb')
    pdfWriter.write(resultPdf)
    resultPdf.close()

# die Funktion speoichert eine XML Datei in einen Ordner
def save_xml_attachement_to_folder(attachementData, folder_path= ORDER_PATH ):
    doc = ET.fromstring(attachementData[1].decode("utf-8"))
    tree = ET.ElementTree(doc)
    tree.write(folder_path + generate_unique_identifier() + attachementData[0],encoding="utf-8")

# die Funktion stellt eine Verbindung zu Gmail her
def get_gmail_inbox_connection():   
    user = "saschasuper55@gmail.com"
    host = "imap.gmail.com"
    mailbox = "inbox"
    password = "saschaSuper8!"
    return easyimap.connect(host, user, password, mailbox)


def main():
    create_folder_structure()
    gmail_inbox = get_gmail_inbox_connection()
    
    for mail in gmail_inbox.unseen(): # durchlaufe alle ungelesenen eMails
        if mail.title != "Bestellung": # prüfe ob der mail titel als Bestellung bezeichet wurde
            continue
        for attachement in mail.attachments: # falls Bestellung, durchlaufe alla Anhänge
            if attachement[0].endswith('.pdf'): # handle PDF dateien
              save_pdf_attachement_to_folder(attachement)
              
            elif attachement[0].endswith('.xml'): # handle XML dateien
              save_xml_attachement_to_folder(attachement)

main()
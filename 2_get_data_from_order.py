# -*- coding: utf-8 -*-

from automagica import *
from tika import parser
import pandas as pd
import tabula , re, os
import xml.etree.ElementTree as ET


DESKTOP = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop') 
DATA_PATH = DESKTOP + r"\Bestellungen\CSV\\"
ORDER_PATH = DESKTOP + r"\Bestellungen\neu"


# die Funktion erstellt eine Ordnerstruktur auf dem Desktop
def create_folder_structure():
    if not os.path.exists(DATA_PATH):
        os.makedirs(DATA_PATH)

# die Funktion prüft, ob es sich um eine Ostseerad_Gmbh pdf Bestellung handelt
def is_Ostseerad_Gmbh_pdf(order):
    table = tabula.read_pdf(order,pages="all")
    try:
        return (table[0].Kundennummer == 20001).bool()
    except:
        return False

# die Funktion exportiertdie Daten der PDF Bestellung in eine CSV Datei
def export_Ostseerad_Gmbh_pdf(order):
    #get tables from order
    table = tabula.read_pdf(order,pages="all")
    #get text from order
    raw = parser.from_file(order)
    email = re.findall('\S+@\S+', raw['content'])[0] 
    orderNumber = str(table[0].Bestellnummer[0])
    customerData = table[0].copy()
    customerData.insert(4,"Email",[email],True)
    table.pop(0)
    order_df = pd.concat(table, ignore_index=True)
    order_df = order_df.join(customerData).fillna(method='ffill')
    order_df.to_csv ("{}\{}.csv".format(DATA_PATH, orderNumber), index = False, header=True)
    
# die Funktion prüft, ob es sich um eine Ostseerad_Gmbh xml Bestellung handelt
def is_Ostseerad_Gmbh_xml(root):
    try:
        return root.find("./General/Sell_to_Customer/Customer_Number").text == "20001"
    except:
        return False

# die Funktion exportiertdie Daten der XML Bestellung in eine CSV Datei
def export_Ostseerad_Gmbh_xml(root):
    column = ["Pos","Art-Nummer","BESCHREIBUNG","Menge","STÜCKPREIS in €","SUMME in €","Bestelldatum","Bestellnummer","Kundennummer","Wunsch-Lieferdatum","Email"]
    dataArray = []
    bestellDatum = root.find("./General/Order_Date").text
    bestellNum = root.find("./General/Customer_Order_Number").text
    kundenNum = root.find("./General/Sell_to_Customer/Customer_Number").text
    lieferDatum = root.find("./Shipping/Ship_to_Customer/Ship_Date").text
    email = root.find("./Shipping/Ship_to_Customer/Email").text
    
    for element in root.iter('Line'):
        pos = element.find("./POS").text
        artNr = element.find("./Common_Item_Number").text
        beschreibung = element.find("./Common_Item_Name").text
        menge = element.find("./Quantity").text
        preis = element.find("./Common_Item_Price").text
        sum = element.find("./Price_Sum").text
        dataArray.append([pos,artNr,beschreibung,menge,preis,sum,bestellDatum,bestellNum,kundenNum,lieferDatum,email])

    order_df = pd.DataFrame(dataArray, columns = column) 
    order_df.to_csv ("{}\{}.csv".format(DATA_PATH, bestellNum), index = False, header=True)

# die Funktion verarbeitet PDF Bestellungen (an dieser Stelle könnten weitere Kundenbestellungen hinzugefügt werden)
def handle_type_of_order_pdf(order):
    if is_Ostseerad_Gmbh_pdf(order) :
        export_Ostseerad_Gmbh_pdf(order)

# die Funktion verarbeitet XML Bestellungen (an dieser Stelle könnten weitere Kundenbestellungen hinzugefügt werden)
def handle_type_of_order_xml(order):
    tree = ET.parse(order)
    root = tree.getroot()
    if is_Ostseerad_Gmbh_xml(root) :
        export_Ostseerad_Gmbh_xml(root)

def main():
    create_folder_structure()
    orders = get_files_in_folder(ORDER_PATH, show_full_path=True, scan_subfolders=False)
    for order in orders: # durchlaufe alle Bestellungen
        if order[-4:] == ".pdf":
            handle_type_of_order_pdf(order)
        elif order[-4:] == ".xml":
            handle_type_of_order_xml(order)
            
main()